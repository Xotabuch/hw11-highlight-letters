const buttons = document.querySelectorAll(".btn");

window.addEventListener("keydown", (e) => {
  console.log(e.code);
  buttons.forEach((element) => {
    element.style.backgroundColor = "#000000";
    if (
      element.textContent.toUpperCase() === e.key.toUpperCase() ||
      `Key${element.textContent}` === e.code
    ) {
      element.style.backgroundColor = "blue";
    }
  });
});
